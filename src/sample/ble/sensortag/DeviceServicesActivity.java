package sample.ble.sensortag;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import sample.ble.sensortag.adapters.TiServicesAdapter;
import sample.ble.sensortag.config.AppConfig;
import sample.ble.sensortag.fusion.SensorFusionActivity;
import sample.ble.sensortag.sensor.TiSensor;
import sample.ble.sensortag.sensor.TiSensors;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * For a given BLE device, this Activity provides the user interface to connect,
 * display data, and display GATT services and characteristics supported by the
 * device. The Activity communicates with {@code BleService}, which in turn
 * interacts with the Bluetooth LE API.
 */
public class DeviceServicesActivity extends BleServiceBindingActivity implements
		ExpandableListView.OnChildClickListener,
		TiServicesAdapter.OnServiceItemClickListener, IMessageArrivedListener {
	
	private final static String TAG = DeviceServicesActivity.class
			.getSimpleName();

	public EditText dataField;
	private ExpandableListView gattServicesList;
	private TiServicesAdapter gattServiceAdapter;

	private TiSensor<?> activeSensor;
	private ChatService mService;
	private ScrollView mScrollView;
	private String msg="";
	private boolean mBound,isRead;
	private Button clear;

	final Handler m_handler = new Handler();
	Timer timer = new Timer();
	MyTimerTask myTimerTask;

	private List<BluetoothGattService> services;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.device_services_activity);

		gattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
		gattServicesList.setOnChildClickListener(this);
		final View emptyView = findViewById(R.id.empty_view);
		gattServicesList.setEmptyView(emptyView);

		mScrollView = (ScrollView) findViewById(R.id.textAreaScroller);
		dataField = (EditText) findViewById(R.id.data_value);
		clear = (Button) findViewById(R.id.clear);

		getActionBar().setTitle(getDeviceName());
		getActionBar().setSubtitle(getDeviceAddress());
		getActionBar().setDisplayHomeAsUpEnabled(true);
		Intent startServiceIntent = new Intent(this, ChatService.class);
		startService(startServiceIntent);

		myTimerTask = new MyTimerTask();
		timer.schedule(myTimerTask, 1000, 1000);
		clear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				msg="";
				dataField.setText(msg);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		final String deviceName = getDeviceName();
		if (deviceName != null) {
			getMenuInflater().inflate(R.menu.gatt_services, menu);

			// enable demo for SensorTag device only
			menu.findItem(R.id.menu_demo).setEnabled(
					deviceName.startsWith(AppConfig.BLE_DEVICE_NAME));

		}
		return true;
	}

	@Override
	protected void onStart() {
		super.onStart();

		Intent intent = new Intent(this, ChatService.class);
		bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_demo:
			final Intent demoIntent = new Intent();
			demoIntent.setClass(DeviceServicesActivity.this,
					SensorFusionActivity.class);
			demoIntent.putExtra(SensorFusionActivity.EXTRA_DEVICE_ADDRESS,
					getDeviceAddress());
			startActivity(demoIntent);
			break;
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDisconnected() {
		finish();
	}

	@Override
	public void onServiceDiscovered() {
		// Show all the supported services and characteristics on the user
		// interface.
		services = getBleService().getSupportedGattServices();

		displayGattServices(services);
		// try{
		// final BluetoothGattCharacteristic characteristic =
		// gattServiceAdapter.getChild(2, 0);
		// final TiSensor<?> sensor =
		// TiSensors.getSensor(characteristic.getService().getUuid().toString());
		// if (sensor == activeSensor)
		// return ;
		//
		// activeSensor = sensor;
		// }catch(Exception e){
		// e.printStackTrace();
		// }

	}

	@Override
	public void onDataAvailable(String serviceUuid, String characteristicUUid,
			String text, byte[] data) {
		// Map<String,String> map = new HashMap<String,String>();
		// map.put("uuid", ""+serviceUuid);
		// map.put("cuuid", ""+characteristicUUid);
		// map.put("msg", ""+text);
		// JSONObject obj = new JSONObject(map);
		String str = text;
		str = str.replaceAll("[^\\d.]", "");
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		if(!str.startsWith("00")){
		msg += "\n"+text +"\n"+date+"\n*******************************";
		dataField.setText(msg);
		isRead = false;
		 mScrollView.post(new Runnable() {
	            @Override
	            public void run() {
	                mScrollView.fullScroll(View.FOCUS_DOWN);
	            }
	        });
		}else{
			if(!isRead){
			isRead = true;
			msg += "\n"+text +"\n"+date+"\n*******************************";
			dataField.setText(msg);
			
			 mScrollView.post(new Runnable() {
		            @Override
		            public void run() {
		                mScrollView.fullScroll(View.FOCUS_DOWN);
		            }
		        });
			}
		}
		// m_handler.post(m_handlerTask);
		//	send(text);
		
	}

	void send(String data) {
		if (mService != null)
			mService.sendMessage("" + data);
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		if (gattServiceAdapter == null)
			return false;

		final BluetoothGattCharacteristic characteristic = gattServiceAdapter
				.getChild(groupPosition, childPosition);
		final TiSensor<?> sensor = TiSensors.getSensor(characteristic
				.getService().getUuid().toString());

		
		if (activeSensor != null)
			getBleService().enableSensor(activeSensor, false);

		if (sensor == null) {
			getBleService().getBleManager().readCharacteristic(characteristic);
			return true;
		}
		Log.e("", ""+sensor.getName());

		if (sensor == activeSensor)
			return true;

		activeSensor = sensor;
		getBleService().enableSensor(sensor, true);
		return true;
	}

	@Override
	public void onServiceUpdated(BluetoothGattService service) {
		final TiSensor<?> sensor = TiSensors.getSensor(service.getUuid()
				.toString());
		if (sensor == null)
			return;
		
		getBleService().updateSensor(sensor);
	}

	private void displayGattServices(List<BluetoothGattService> gattServices) {
		if (gattServices == null)
			return;

		gattServiceAdapter = new TiServicesAdapter(this, gattServices);
		gattServiceAdapter.setServiceListener(this);
		gattServicesList.setAdapter(gattServiceAdapter);
	}

	@Override
	public void messageArrived(String message) {
		// TODO Auto-generated method stub
		dataField.setText("" + message);

	}

	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			ServiceBinder binder = (ServiceBinder) service;
			mService = binder.getService();
			binder.setMessageArrivedListener(DeviceServicesActivity.this);
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mBound = false;
		}
	};

	class MyTimerTask extends TimerTask {

		@Override
		public void run() {

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					 if(gattServiceAdapter!=null){
					 final BluetoothGattCharacteristic characteristic =gattServiceAdapter.getChild(2, 0);

					
						getBleService().getBleManager().readCharacteristic(characteristic);
					
					 }
				}
			});

		}
	}
}
